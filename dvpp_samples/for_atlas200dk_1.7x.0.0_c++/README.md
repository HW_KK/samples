# dvpp_samles-Atlas200DK

#### 介绍
本仓包含Atlas200支持的媒体数据处理功能，各文件夹对应不同功能，以供用户参考。具体说明如下。

1. [venc](https://gitee.com/ascend/samples/blob/master/dvpp_samples/for_atlas200dk_1.7x.0.0_c++/venc/readme.md)：将YUV420SP NV12/NV21-8bit图片数据编码成H264/H265格式的视频码流，不支持单进程多线程场景。



