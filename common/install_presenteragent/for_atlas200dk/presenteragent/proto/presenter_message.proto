// Copyright 2020 Huawei Technologies Co., Ltd
//
// Copyright 2020, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

syntax = "proto3";

package ascend.presenter.proto;

enum OpenChannelErrorCode {
    kOpenChannelErrorNone = 0;
    kOpenChannelErrorNoSuchChannel = 1;
    kOpenChannelErrorChannelAlreadyOpened = 2;
    kOpenChannelErrorOther = -1;
}

enum ChannelContentType {
    kChannelContentTypeImage = 0;
    kChannelContentTypeVideo = 1;
}

// By Protocol Buffer Style Guide, need to use underscore_separated_names
// for field names
message OpenChannelRequest {
    string channel_name = 1;
    ChannelContentType content_type = 2;
}

message OpenChannelResponse {
    OpenChannelErrorCode error_code = 1;
    string error_message = 2;
}

message HeartbeatMessage {

}

enum ImageFormat {
    kImageFormatJpeg = 0;
}

message Coordinate {
    uint32 x = 1;
    uint32 y = 2;
}

message Rectangle_Attr {
    Coordinate left_top = 1;
    Coordinate right_bottom = 2;
    string label_text = 3;
}

message PresentImageRequest {
     ImageFormat format = 1;
     uint32 width = 2;
     uint32 height = 3;
     bytes data = 4;
     repeated Rectangle_Attr rectangle_list = 5;
}

enum PresentDataErrorCode {
    kPresentDataErrorNone = 0;
    kPresentDataErrorUnsupportedType = 1;
    kPresentDataErrorUnsupportedFormat = 2;
    kPresentDataErrorOther = -1;
}

message PresentImageResponse {
    PresentDataErrorCode error_code = 1;
    string error_message = 2;
}

