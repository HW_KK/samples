中文|[英文](README_EN.md)

# install_python3env

#### 介绍

install_python3env，请根据自己的开发环境选择对应文件夹，或者点击下面的说明链接选择需要的python3env安装指导文档。

#### 使用说明

1. [for_atlas200dk](https://gitee.com/ascend/samples/tree/master/common/install_python3env/for_atlas200dk)

   atlas200dk环境安装python3env指导文档。

2. [for_atlas300](https://gitee.com/ascend/samples/tree/master/common/install_python3env/for_atlas300)

   atlas300环境安装python3env指导文档。